package by.training.mthreadingtask;

import java.util.Map;

/***
 * Class shows LRU/LFU caches.
 */
public class Runner {

    public static void main(String[] args) {

        int maxEntries = 4;

        Map<Integer, String> lruCache = new LRUCache<Integer, String>(maxEntries);

        for (int i = 0; i < maxEntries; i++) lruCache.put(i, String.valueOf(i));

        System.out.println("-------LRUCache-------");
        System.out.println(lruCache);

        System.out.println("---put 2--");
        lruCache.put(2, String.valueOf(2));
        System.out.println(lruCache);

        System.out.println("---get 0--");
        lruCache.get(0);
        System.out.println(lruCache);

        System.out.println("---put 7--");
        lruCache.put(7, String.valueOf(7));
        System.out.println(lruCache);

        Map lfuCache = new LFUCache<Integer, String>(maxEntries, 0.2f);

        for (int i = 0; i < maxEntries; i++)
            lfuCache.put(i, i);

        System.out.println("\n------- LFUCache -------");
        System.out.println(lfuCache);

        System.out.print("---get 0--");
        lfuCache.get(0);
        System.out.println(lfuCache);

        System.out.print("---put 13--");
        lfuCache.put(13, 13);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 2 --");
        lfuCache.get(2);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 0--");
        lfuCache.get(0);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 0--");
        lfuCache.get(0);
        System.out.println(lfuCache);

        System.out.print("---get 2 --");
        lfuCache.get(2);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);

        System.out.print("---get 2 --");
        lfuCache.get(2);
        System.out.println(lfuCache);

        System.out.print("---get 13 --");
        lfuCache.get(13);
        System.out.println(lfuCache);

        System.out.print("---put 10 --");
        lfuCache.put(10, 10);
        System.out.println(lfuCache);

        System.out.print("---get 2 --");
        lfuCache.get(2);
        System.out.println(lfuCache);

        System.out.print("---get 2 --");
        lfuCache.get(2);
        System.out.println(lfuCache);

        System.out.print("---get 3 --");
        lfuCache.get(3);
        System.out.println(lfuCache);
    }
}
