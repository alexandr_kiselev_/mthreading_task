package by.training.mthreadingtask;

import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LFUCache realisation.
 *
 * @param <K>
 * @param <V>
 */
public class LFUCache<K, V> implements Map<K, V> {

    private final Map<K, CacheNode<K, V>> cache;
    private final Set[] frequencySet;
    private int lowestFrequency;
    private int maxFrequency;
    private final float evictionFactor;
    private final int maxCacheSize;
    private final Lock lock;

    /***
     * Constructor LFUCache.
     * @param maxCacheSize .
     * @param evictionFactor .
     */
    public LFUCache(final int maxCacheSize, final float evictionFactor) {
        this.cache = new Hashtable<K, CacheNode<K, V>>(maxCacheSize);
        this.frequencySet = new Set[maxCacheSize + 1];
        this.lowestFrequency = 0;
        this.evictionFactor = evictionFactor;
        this.maxFrequency = maxCacheSize;
        this.maxCacheSize = maxCacheSize;
        initFrequencySet();
        lock = new ReentrantLock();
    }

    /**
     * Initialize array frequencySet.
     */
    private void initFrequencySet() {
        for (int i = 0; i <= maxFrequency; i++) {
            frequencySet[i] = Collections.synchronizedSet(new LinkedHashSet<CacheNode<K, V>>());
        }
    }

    /***
     * Insert new pair (key, value) in cache with frequency 0.
     * @param k key
     * @param v value
     * @return old value
     */
    public V put(final K k, final V v) {
        V oldValue = null;
        lock.lock();
        try {
            CacheNode<K, V> currentNode = cache.get(k);
            if (currentNode == null) {
                if (cache.size() == maxCacheSize) {
                    eviction();
                }
                Set<CacheNode<K, V>> nodes = frequencySet[0];
                currentNode = new CacheNode(k, v, 0);
                nodes.add(currentNode);
                cache.put(k, currentNode);
                lowestFrequency = 0;
            } else {
                oldValue = currentNode.v;
                currentNode.v = v;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            return oldValue;
        }
    }

    /**
     * Override method putAll.
     *
     * @param map
     */
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        for (Entry<? extends K, ? extends V> me : map.entrySet()) {
            put(me.getKey(), me.getValue());
        }
    }


    /**
     * Get value on the key from cache and up frequency.
     *
     * @param k key
     * @return value or null
     */
    public V get(final Object k) {
        V returnValue = null;
        lock.lock();
        try {
            CacheNode<K, V> currentNode = cache.get(k);
            if (currentNode != null) {
                int currentFrequency = currentNode.frequency;
                if (currentFrequency < maxFrequency) {
                    int nextFrequency = currentFrequency + 1;
                    Set<CacheNode<K, V>> currentNodes = frequencySet[currentFrequency];
                    Set<CacheNode<K, V>> newNodes = frequencySet[nextFrequency];
                    moveToNextFrequency(currentNode, nextFrequency, currentNodes, newNodes);
                    cache.put((K) k, currentNode);
                    if (lowestFrequency == currentFrequency && currentNodes.isEmpty()) {
                        lowestFrequency = nextFrequency;
                    }
                } else {
                    Set<CacheNode<K, V>> nodes = frequencySet[currentFrequency];
                    nodes.remove(currentNode);
                    nodes.add(currentNode);
                }
                returnValue = currentNode.v;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            return returnValue;
        }
    }


    /**
     * Remove value on the key from cache with lowest frequency.
     *
     * @param k key
     * @return value or null
     */
    public V remove(final Object k) {
        CacheNode<K, V> currentNode = cache.remove(k);
        if (currentNode != null) {
            Set<CacheNode<Integer, String>> nodes = frequencySet[currentNode.frequency];
            nodes.remove(currentNode);
            if (lowestFrequency == currentNode.frequency) {
                findNextLowestFrequency();
            }
            return currentNode.v;
        } else {
            return null;
        }
    }

    /**
     * Clear array frequencySet.
     */
    public void clear() {
        for (int i = 0; i <= maxFrequency; i++) {
            frequencySet[i].clear();
        }
        cache.clear();
        lowestFrequency = 0;
    }

    /**
     * Return size.
     *
     * @return .
     */
    public int size() {
        return cache.size();
    }

    /**
     * If cache is empty.
     *
     * @return true
     */
    public boolean isEmpty() {
        return this.cache.isEmpty();
    }

    /**
     * If contain key.
     *
     * @param o key
     * @return true
     */
    public boolean containsKey(final Object o) {
        return this.cache.containsKey(o);
    }

    /**
     * Allways return false.
     *
     * @param o value
     * @return false
     */
    public boolean containsValue(final Object o) {
        return false;
    }


    /**
     * Return string with all cache values.
     *
     * @return
     */
    @Override
    public java.lang.String toString() {
        java.lang.String returner = "";

        for (int i = 0; i <= maxFrequency; i++) {
            returner = returner + "\n " + i + " --- ";


            Iterator<CacheNode<K, V>> it = frequencySet[i].iterator();
            while (it.hasNext()) {
                CacheNode<K, V> node = it.next();
                returner = returner + " " + node.k + "," + node.v;
            }
        }

        return returner;
    }

    /**
     * Calculate number of entries to delete = max size * eviction factor.
     * Remove values from cache.
     */
    private void eviction() {
        int currentlyDeleted = 0;
        float target = evictionFactor * maxCacheSize;
        while (currentlyDeleted < target) {
            Set<CacheNode<K, V>> nodes = frequencySet[lowestFrequency];
            if (nodes.isEmpty()) {
                throw new IllegalStateException("Lowest frequency constraint violated!");
            } else {
                Iterator<CacheNode<K, V>> it = nodes.iterator();
                while (it.hasNext() && currentlyDeleted++ < target) {
                    CacheNode<K, V> node = it.next();
                    it.remove();
                    cache.remove(node.k);
                }
                if (!it.hasNext()) {
                    findNextLowestFrequency();
                }
            }
        }
    }

    /**
     * Return keySet.
     *
     * @return .
     */
    public Set<K> keySet() {
        return this.cache.keySet();
    }

    /***
     * Allways return null.
     * @return .
     */
    public Collection<V> values() {
        return null;
    }

    /***
     * Allways return null.
     * @return .
     */
    public Set<Entry<K, V>> entrySet() {
        return null;
    }


    /***
     * Remove from current frequencySet and insert in frequencySet where frequency+1.
     * @param currentNode .
     * @param nextFrequency .
     * @param currentNodes .
     * @param newNodes .
     */
    private void moveToNextFrequency(final CacheNode<K, V> currentNode, final int nextFrequency,
                                     final Set<CacheNode<K, V>> currentNodes,
                                     final Set<CacheNode<K, V>> newNodes) {
        currentNodes.remove(currentNode);
        newNodes.add(currentNode);
        currentNode.frequency = nextFrequency;
    }

    /**
     * Find and set lowestFrequency.
     */
    private void findNextLowestFrequency() {
        while (lowestFrequency <= maxFrequency && frequencySet[lowestFrequency].isEmpty()) {
            lowestFrequency++;
        }
        if (lowestFrequency > maxFrequency) {
            lowestFrequency = 0;
        }
    }

    /***
     * Class cache node key, value and frequency.
     * @param <Key>
     * @param <Value>
     */
    private static class CacheNode<Key, Value> {

        private final Key k;
        private Value v;
        private int frequency;

        CacheNode(final Key k, final Value v, final int frequency) {
            this.k = k;
            this.v = v;
            this.frequency = frequency;
        }

    }
}