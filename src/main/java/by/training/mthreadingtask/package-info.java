/**
 * Collection task contains LFU and LRU cache realisations
 * @since 1.0
 * @author Alexandr_Kiselev
 * @version 1.0
 *
 */
package by.training.mthreadingtask;