package by.training.mthreadingtask;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LRUCache realisation.
 *
 * @param <K>
 * @param <V>
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private final Lock lock;
    /**
     * Count maximum entries.
     */
    private final int maxEntries;

    /**
     * LRUCache constructor.
     *
     * @param maxEntries
     */
    LRUCache(final int maxEntries) {
        super(maxEntries + 1, 1.0f, true);
        this.maxEntries = maxEntries;
        lock = new ReentrantLock();
    }

    /**
     * Lock before get() and unlock after
     *
     * @param key
     * @return
     */
    @Override
    public V get(final Object key) {
        V returnValue = null;
        lock.lock();
        try {
            returnValue = super.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            return returnValue;
        }
    }

    /**
     * Lock before put() and unlock after
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public V put(K key, V value) {
        V returnValue = null;
        lock.lock();
        try {
            returnValue = super.put(key, value);
        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            lock.unlock();
            return returnValue;
        }
   }

    /**
     * Return true for remove eldest entry if no more free space.
     *
     * @param eldest
     * @return
     */
    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return super.size() > maxEntries;
    }

    /**
     * Return string with all cache values.
     *
     * @return
     */
    @Override
    public java.lang.String toString() {
        java.lang.String returner = "";

        for (Map.Entry<K, V> me : this.entrySet()) {
            returner = returner + " " + me.getValue();
        }

        return returner;
    }
}
